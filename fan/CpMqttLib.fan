/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//

using haystack
using hisExt
using connExt
using util
using axon
using mqtt
using crypto
using inet
using skyarcd

**
** CpMqtt library functions.
**
const class CpMqttLib
{  

  **
  ** Ping a CpMqtt connector 
  **
  ** Examples:
  **   read(cpMqttConn).cpMqttPing
  **   cpMqttPing(cpMqttConnId)
  **
  ** Side effects:
  **   - Obtains the Version for the MQTT Broker this connector is connected to
  **
  @Axon { admin = true }
  static Obj? cpMqttPing(Obj conn)
  {
    CpMqttExt.cur.connActor(conn).ping
    return null
  }

  **
  ** Subscribe all the proxy points to their MQTT topic on the MQTT Broker.
  ** Current value with asynchronously arrive on the next message received for the topic.
  ** If there is a retained message for the topic, current value will arrive almost immediately.
  ** The proxies may be any value supported by `toRecList`.
  **
  ** Each proxy point must contain:
  **  - `cpMqttConnRef`:  references the CpMqtt connector
  **  - `cpMqttCur`: Topic Str used for current value
  **
  ** Side effects:
  **   - performs async network IO to subscribe to the point val topics
  **   - updates `curVal` tag transiently
  **   - updates `curStatus` tag transiently
  **   - updates `curErr` tag transiently
  **
  @Axon { admin = true }
  static Obj? cpMqttSyncCur(Obj proxies)
  {
    CpMqttExt.cur.syncCur(proxies)
  }
  
  **
  ** MQTT message parser that is passed the payload as a String and uses it as the point curVal.
  ** It assumes point curStatus is "ok". 
  ** Units should not be in the message body.
  ** 
  ** If no MQTT parser is configured on an CpMqtt Connector or point, this
  ** is the default parser used.
  ** 
  ** This function abides by the contract for MQTT message parsers in taking
  ** a Str as its first parameter and returning a Dict with curVal and curStatus tags.
  **
  @Axon 
  static Dict cpMqttParseVal(Str msg)
  {
    tags := Str:Obj[:]
    tags["curVal"] = msg
    tags["curStatus"] = "ok"
    return Etc.makeDict(tags)
  }

  **
  ** MQTT message parser that is passed the payload as a String and parses it as a comma separated set of values.
  ** 
  ** Default column separator is ',' and expected value positions are '"curVal","curStatus"'
  ** To override this pass in a Dict as an optional second parameter.
  ** The Dict should contain the tags colSep and colNames. 
  ** colSep is a single character Str and colNames is Str[]
  ** 
  ** If there are more values than column names supplied, default colnames of v1..vn are used.
  ** If curStatus is not one of the column names curStatus will be set to "ok"
  ** 
  ** This function abides by the contract for MQTT message parsers in taking
  ** a Str as its first parameter and returning a Dict with curVal and curStatus tags.
  **
  ** Configure it on the connector either just as the function name or follow it with a Dict 
  ** containing colSep and colNames tags.
  ** e.g. 
  **   cpMqttParseCsv {colSep:",", colNames:["curVal","curStatus"]}
  ** 
  @Axon 
  static Dict cpMqttParseCsv(Str msg, Dict? params := null)
  {
    colSep := ","
    colNames := ["curVal", "curStatus"]      
    if (params != null && params["colSep"] != null) colSep = params["colSep"]
    if (params != null && params["colNames"] != null) colNames = params["colNames"]
    
    colVals := msg.split(colSep.chars[0])   
        
    tags := Str:Obj[:]
    colVals.each |val, idx| 
    {
      tagName := "v" + idx
      if (idx < colNames.size) tagName = colNames[idx]      
      tags[tagName] = val
    }
    if (tags["curStatus"] == null) tags["curStatus"] = "ok"
    
    return Etc.makeDict(tags)
  }

  **
  ** MQTT message parser that is passed the payload as a String and parses it as a JSON object.
  ** 
  ** The JSON Object structure is fixed. 
  ** It expects the outer object to contain an array of metric objects, with the property name metrics.
  ** Each metric object in the array must have a name and a value property.
  ** 
  ** Example JSON Message:
  **    
  **    {
  **       "metrics":
  **      [
  **          {"name":"pt1","value":73.0},
  **          {"name":"pt2","value":true},
  **          {"name":"pt3","value":"string val"}
  **      ]
  **    }
  ** 
  ** results in this Dict:
  **   {pt1:73.0, pt2:true, pt3:"string val"}
  **
  @Axon 
  static Dict cpMqttParseJson(Str msg, Dict? params := null)
  {
    Str:Obj data := JsonInStream(msg.in).readJson
    metrics := data["metrics"] as Obj[]
    tags := Str:Obj[:]
    metrics.each | m |
    {  
      metric := m as Str:Obj
      val := metric["value"]
      if (val.typeof == Float#)
      {
        tags[metric["name"]] = Number((Float)val)
      }
      else
      {
        tags[metric["name"]] = val
      }
    }
    return Etc.makeDict(tags)
  }

  **
  ** MQTT message constructor that is passed the point record, value and level
  ** 
  ** The JSON Str returned is an outer object containing an array with the property name metrics.
  ** The single object in the array has a name and a value property, and optionally a level property if provided
  ** 
  ** Example returned JSON Message:
  **    
  **    {
  **       "metrics":
  **      [
  **          {"name":"pt1","value":73.0,"level":1}
  **      ]
  **    }  
  @Axon
  static Str cpMqttConstructJson(Dict pt, Obj? val, Number? level := null)
  {
    Str? mqttPointName := "curVal"
    if (pt["cpMqttPointName"] != null) mqttPointName = pt["cpMqttPointName"] as Str
    
    Str? valStr
    if(val == null)
    {
      valStr = "\"NULL\""
    }
    else
    {
      valStr = val.toStr 
      if (val.typeof == Number#)
      {
        valStr = (val as Number).toFloat.toStr
      }
      else if (val.typeof == Str#)
      {
        valStr = "\"$valStr\""
      }
    }
    Str levelStr := ""
    if (level != null)
    {
        levelStr = ", \"level\":" + level.toFloat.toStr
    }
    return "{\"metrics\":[{\"name\":\"$mqttPointName\",\"value\":$valStr $levelStr}]}"
  }

  **
  ** Create an instance of an MqttClient, connect and return it
  ** This is unrelated to Connector functionality
  ** The Dict parameter provided may contain the following
  ** url - (Str or Uri). Format is mqtt://host:port or mqtts://host:port
  ** username
  ** password - Plain Text
  ** certFile - If client cert file auth is being used (alternative to username/password)
  ** keyFile - Must be supplied if certFile is used
  ** clientId - to override a default UUID generated client ID
  **
  @Axon
  static MqttClient cpMqttClientConnect(Dict cfg)
  {
    mqttBroker := cfg->url.toStr
    if (mqttBroker.startsWith("tcp:")) mqttBroker = "mqtt:" + mqttBroker[4..-1]
    else if (mqttBroker.startsWith("ssl:")) mqttBroker = "mqtts:" + mqttBroker[4..-1]

    clientId := cfg["clientId"] ?: "skySpark-" + Uuid().toStr

    Str? username  := cfg["username"] ?: ""
    Str? password  := cfg["password"] ?: ""

    Str? clientCertFile := cfg["certFile"]
    Str? clientKeyFile := cfg["keyFile"]
    SocketConfig? sc := null
    if (clientCertFile != null && clientKeyFile != null)
    {
        basePath := Context.cur.proj.dir.pathStr
        clientCertFile = basePath + "/data/mqtt/" + clientCertFile
        clientKeyFile = basePath + "/data/mqtt/" + clientKeyFile

        bc := Crypto.cur.loadPem(Uri.fromStr(clientCertFile).toFile.in)
        bk := Crypto.cur.loadPem(Uri.fromStr(clientKeyFile).toFile.in)
        sc = SocketConfig.cur.copy {
          it.keystore = Crypto.cur.loadKeyStore.setPrivKey("", bk, [bc])
          it.connectTimeout = 10sec
          it.receiveTimeout = null
          it.tlsParams = ["appProtocols": ["x-amzn-mqtt-ca"]]
        }
    }

    client := MqttClient(ClientConfig {
      it.serverUri = Uri.fromStr(mqttBroker)
      it.clientId = clientId
      if (sc != null) it.socketConfig = sc
    })

    client.connect(ConnectConfig {
      if ("" != username)
      {
        it.username = username
        it.password = password.toBuf
      }
    }).get(10sec)

    return client

  }

  **
  ** Provide a function that publishes a message to a broker on a topic
  ** This is unrelated to Connector functionality
  ** The Dict parameter provided must contain the following
  ** url - (Str or Uri). Format is mqtt://host:port or mqtts://host:port
  ** topic - What topic to publish to
  ** msg - Supports String messages only
  **
  ** Optionally you can also provide:
  ** username
  ** password - Plain Text
  ** certFile - If client cert file auth is being used (alternative to username/password)
  ** keyFile - Must be supplied if certFile is used
  ** clientId - to override a default UUID generated client ID
  ** qos - Int 0|1|2
  **
  @Axon
  static Void cpMqttPublishMsg(Dict cfg)
  {
    client := cpMqttClientConnect(cfg)

    qos := QoS.one
    if (cfg.has("qos"))
    {
        qosInt := (cfg->qos as Number).toInt
        if (qosInt == 0) qos = QoS.zero
        else if (qosInt == 1) qos = QoS.one
        else if (qosInt == 2) qos = QoS.two
    }

    Str? msg := cfg->msg as Str
    client.publishWith
          .topic(cfg->topic)
          .qos(qos)
          .payload(msg.toBuf)
          .send.get(10sec)

    cpMqttClientClose(client)
  }

  **
  ** Publish a message to the specified topic using the provided MqttClient.
  ** Return the same provided client to it can be used again in a chain of commands
  **
  @Axon
  static MqttClient cpMqttClientPublish(MqttClient client, Str msg, Str topic, Number qosNum)
  {
    qos := QoS.one
    qosInt := qosNum.toInt
    if (qosInt == 0) qos = QoS.zero
    else if (qosInt == 1) qos = QoS.one
    else if (qosInt == 2) qos = QoS.two

    client.publishWith
          .topic(topic)
          .qos(qos)
          .payload(msg.toBuf)
          .send.get(10sec)

    return client
  }

  **
  ** Close the provided MqttClient
  **
  @Axon
  static Void cpMqttClientClose(MqttClient client)
  {
      client.disconnect.get(10sec)
  }

}

