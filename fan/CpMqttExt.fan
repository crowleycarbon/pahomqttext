/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//

using hisExt
using connExt
using skyarcd

**
** CpMqtt Extension
**
@ExtMeta
{
  name       = "cpMqtt"
  depends    = ["conn"]
  licFeature = "conn"
}
const class CpMqttExt : ConnImplExt
{

  //////////////////////////////////////////////////////////////////////////
  // Current
  //////////////////////////////////////////////////////////////////////////

  **
  ** Get CpMqttExt for the current context.
  **
  static CpMqttExt? cur(Bool checked := true)
  {
    Context.cur.ext("cpMqtt", checked)
  }

  //////////////////////////////////////////////////////////////////////////
  // Constructor
  //////////////////////////////////////////////////////////////////////////

  @NoDoc new make( ) : super( CpMqttModel())
  {
  }
  
}