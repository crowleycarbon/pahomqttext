/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//

using mqtt
using haystack
using connExt
using concurrent
using crypto
using inet
using folio
using skyarcd
using skyarc

**
** CpMqttConn
**
class CpMqttConn : Conn
{  
  new make(ConnActor actor, Dict rec) : super(actor, rec)
  {

  }

//////////////////////////////////////////////////////////////////////////
// Messaging
//////////////////////////////////////////////////////////////////////////
  override Obj? receive(ConnMsg msg)
  {
    return super.receive(msg)
  }

//////////////////////////////////////////////////////////////////////////
// Connection
//////////////////////////////////////////////////////////////////////////
  override Void onOpen()
  {
    log.info("onOpen started. Logger name is $log.name")
    
    topicRoot = rec["topicRoot"] ?: ""
    log.info("topicRoot is $topicRoot")
    
    mqttBroker := rec["cpMqttBroker"] ?: throw FaultErr("Missing 'cpMqttBroker' tag")
    mqttBrokerUri := mqttBroker as Uri ?: throw FaultErr("Type of 'cpMqttBroker' must be Uri, not $mqttBroker.typeof.name")

    mqttBrokerStr := mqttBroker.toStr
    if (mqttBrokerStr.startsWith("tcp:")) mqttBrokerStr = "mqtt:" + mqttBrokerStr[4..-1]
    else if (mqttBrokerStr.startsWith("ssl:")) mqttBrokerStr = "mqtts:" + mqttBrokerStr[4..-1]

    clientId := rec["cpMqttClientId"] ?: ("skySpark-" + rec["id"]).replace(":","-")

    if (rec.has("cpMqttQos"))
    {
        qosInt := (rec->cpMqttQos as Number).toInt
        if (qosInt == 0) qos = QoS.zero
        else if (qosInt == 1) qos = QoS.one
        else if (qosInt == 2) qos = QoS.two
    }

    if (rec.has("cpMqttCleanSession")) cleanSession = rec->cpMqttCleanSession
    if (rec.has("retained")) retained = rec->retained
    if (rec.has("keepAlive")) keepAlive = Duration.fromStr(rec->keepAlive.toStr)

    Str? clientCertFile := rec["cpClientCertFile"]
    Str? clientKeyFile := rec["cpClientKeyFile"]
    SocketConfig? sc := null
    if (clientCertFile != null && clientKeyFile != null)
    {
        basePath := proj.dir.pathStr
        clientCertFile = basePath + "/data/mqtt/" + clientCertFile
        clientKeyFile = basePath + "/data/mqtt/" + clientKeyFile

        bc := Crypto.cur.loadPem(Uri.fromStr(clientCertFile).toFile.in)
        bk := Crypto.cur.loadPem(Uri.fromStr(clientKeyFile).toFile.in)
        sc = SocketConfig.cur.copy {
          it.keystore = Crypto.cur.loadKeyStore.setPrivKey("", bk, [bc])
          it.connectTimeout = 10sec
          it.receiveTimeout = null
          it.tlsParams = ["appProtocols": ["x-amzn-mqtt-ca"]]
        }
    }

    client = MqttClient(ClientConfig {
      it.serverUri = Uri.fromStr(mqttBrokerStr)
      it.clientId = clientId
      if (sc != null) it.socketConfig = sc
    })
    .enableAutoReconnect(5sec, 5min)
    .addListener(OnConnectClientListener(this))

    subs = CpMqttPointSubs(this)
    clientConnect()
    log.info("onOpen completed")
  }

  override Void onClose()
  {
    log.info("onClose started")    
    subs.clear
    client.disconnect.get(10sec)
    subs = null
    client = null       
    log.info("onClose completed")
  }

  override Dict onPing()
  {
    tags := Str:Obj[:]
    tags["brokerVersion"] = brokerVersion ?: ""
    return Etc.makeDict(tags)
  }
  
  **
  ** Perform the MQTT Client connection.
  ** MQTT cleanSession config is set.
  ** If username is configured on the connector, use it along with the password.
  ** 
  Void clientConnect()
  {
    log.info("clientConnect started. ")

    brokerUrl := rec["cpMqttBroker"].toStr
    username  := rec["username"] ?: ""
    password  := ext.proj.passwords.get(id.toStr) ?: ""

    client.connect(ConnectConfig {
      it.cleanSession = this.cleanSession
      it.keepAlive = this.keepAlive

      if ("" != username)
      {
        it.username = username
        it.password = password.toBuf
      }
    }).get(10sec)
  }
  
//////////////////////////////////////////////////////////////////////////
// Points
//////////////////////////////////////////////////////////////////////////

  ** Callback for cpMqttSyncCur
  ** There is no way of querying for the current val, so just set up the subscriptions.
  ** 
  ** If there is a retained message on any of the requested topics, curVal will return
  ** almost instantly.
  ** 
  override Void onSyncCur(ConnPoint[] points)
  {
    onWatch(points)
  }

  ** 
  ** Callback for watch.
  ** For each point, subscribe to its topic ('cpMqttCur' tag) 
  ** For each subscription, set the Qos configured at the connector level.
  ** 
  override Void onWatch(ConnPoint[] points)
  {
    log.info("onWatch started for $points.size points")
    points.each |pt|
    {
      topic := pt.rec["cpMqttCur"]
      if (topic != null) subs.addPointTopicSub(topic, pt)
    }    
    subs.subscribeAllTopics()
    log.info("onWatch completed. Topic:Point mapping is - $subs")
  }

  ** 
  ** Callback for unWatch.
  ** For each point, remove it from the list of points subscribing to this topic.
  ** Unsubscribe from any topics at the end for which no points are bound.
  ** 
  override Void onUnwatch(ConnPoint[] points)
  {
    log.debug("onUnwatch started for $points.size points")
    points.each |pt|
    {
      topic := pt.rec["cpMqttCur"]
	  if (subs == null) log.warn("subs is already null!!")
      if (topic != null && subs != null) subs.removePointTopicSub(topic, pt)
    }    
    subs.removeInvalidTopicSubs()
    log.debug("onUnwatch completed. Topic:Point mapping is - $subs")
  }
  
  **
  ** Takes in the raw message payload just received from the topic
  ** and parses it using the configured MQTT Parser, before applying
  ** the value to the points subscribed to this topic.
  ** 
  ** The MQTT Parser can be configured at a Connector rec level using the 'cpMqttParser' tag,
  ** or it can be configured on a Point using the 'cpMqttParser' tag.
  ** If not configured at all, a default raw value parser is used. 
  ** 
  Void handleMessage(Str topic, Str msg)
  {
    
    log.debug("handleMessage. Message received for topic '$topic'")  
    topic = topic.replace(topicRoot, "")
    pts := subs.getTopicPoints(topic)
    if (pts == null || pts.size == 0) return    
    log.debug("handleMessage. There are $pts.size point(s) subscribed to this topic")
    
    //Can parse using the configured parser for the first point as it must has to be the same for all
    Dict? parsed := parseMsg(msg, pts[0])
    
    //null returned is there was a problem parsing the message
    if (parsed == null) return 
    
    log.debug("Parsed Dict is $parsed")
    
    //It's possible for the message to contain the curVal for multiple points
    //all of which are subscribed to the same topic. The Dict can contain vals
    //for all the points using the tag val from the point called 'mqttPointName'
    //as the tag name to look for in the returned Dict from the Parser.
    pts.each |pt| 
    {
      Str? pointName := pt.rec["cpMqttPointName"]
      pointStatusName := "curStatus"
      if (pointName == null) 
      {
        pointName = "curVal"
      }
      else
      {
        pointStatusName = pointName + "CurStatus"
      }
      
      if (parsed[pointStatusName] == null || parsed[pointStatusName] == "ok")
      {
        ptVal := parsed[pointName]
        if (ptVal != null) 
        {          
          pt.updateCurOk(typeConvert(ptVal, pt))
        }
      }
      else
      {
        pt.updateCurErr(Err(parsed[pointStatusName]))        
      }
    }
    log.debug("handleMessage. Completed")
  }   
  
  private Dict? parseMsg(Str msg, ConnPoint pt)
  {
    Str? msgParser := pt.rec["cpMqttParser"]
    if (msgParser == null) msgParser = rec["cpMqttParser"]
    log.debug("parseMsg. Configured Parser is '$msgParser'")        
    
    try
    {
      if (msgParser == null || msgParser == "" || msgParser.startsWith("VAL")) 
      {
        return CpMqttLib.cpMqttParseVal(msg)
      }
      else
      {
        Str functionName := msgParser.split(' ')[0]
        ctx := Context(proj.sys, User.conn, proj)        
        Dict? paramDict := parseParams(ctx, msgParser[(functionName.size) ..-1])
        
        if (functionName.upper == "CSV")
        {
          return CpMqttLib.cpMqttParseCsv(msg, paramDict)
        }
        else if (functionName.upper == "JSON")
        {
          return CpMqttLib.cpMqttParseJson(msg, paramDict)
        }
        else
        {
          params := Obj[,].add(msg)
          if (paramDict != null) params.add(paramDict)
          return ctx.asCur |cx| { cx.call(functionName, params) } as Dict    
        }                           
      }
    }
    catch(Err e)
    {
      log.err("Error parsing MQTT message", e)
      return null
    }
  }

//////////////////////////////////////////////////////////////////////////
// Writes
//////////////////////////////////////////////////////////////////////////
  **
  ** callback for onWrite.
  ** Constructs an MQTT message using the configured message constructor
  ** and publishes it on the MQTT topic configured for this point.
  ** If message construction and publishing succeed, then the updateWriteOk method
  ** is called on the point as a last step.
  **
  override Void onWrite(ConnPoint pt, Obj? val, Number level)
  {
    topic := pt.rec["cpMqttWrite"]
    if (topic == null) return

    log.debug("onWrite. Topic is '$topic', val is '$val' and level is '$level'")
    try
    {
      msg := constructMessage(pt, val, level)
      if (client != null)
      {
        client.publishWith
              .topic(topicRoot + topic)
              .qos(qos)
              .payload(msg.toBuf)
              .send.get(10sec)
        pt.updateWriteOk(val, level)
        log.debug("onWrite. Completed successfully")
      }
      else
      {
        log.info("client no yet initialised")
      }
    }
    catch (Err e)
    {
      log.err("Error onWrite", e)
      //throw e
    }
  }

  private Str constructMessage(ConnPoint pt, Obj? val, Number level)
  {
    Str? mqttConstructor := pt.rec["cpMqttConstructor"]
    if (mqttConstructor == null) mqttConstructor = rec["cpMqttConstructor"]

    Str? msg
    if (mqttConstructor == null || mqttConstructor == "" || mqttConstructor.startsWith("VAL"))
    {
      msg = "$val"
    }
    else
    {
      Str functionName := mqttConstructor.split(' ')[0]
      ctx := Context(proj.sys, User.conn, proj)
      Dict? paramDict := parseParams(ctx, mqttConstructor[(functionName.size) ..-1])

      if (functionName.upper == "JSON")
      {
        msg = CpMqttLib.cpMqttConstructJson(pt.rec, val, level)
      }
      else
      {
        valTags := Str:Obj[:]
        if (val != null) valTags["writeVal"] = val
        valTags["writeLevel"] = level

        params := Obj[,].add(pt)
        params.add(Etc.makeDict(valTags))
        if (paramDict != null) params.add(paramDict)

        msg = ctx.asCur |cx| { cx.call(functionName, params) } as Str
      }
    }
    return msg
  }

//////////////////////////////////////////////////////////////////////////
// Util
//////////////////////////////////////////////////////////////////////////
  private Dict? parseParams(Context ctx, Str paramStr)
  {
    log.debug("parseMsg. paramStr Str is '$paramStr'")  
    if (paramStr.trim.size == 0) return null;    
    try
    {
      return ctx.asCur |cx| { cx.eval("x: " + paramStr) } as Dict
    }
    catch (Err e)
    {
      log.err("Error Parsing Parameter String as Dict :: $paramStr", e)
      return null
    }
  }
  
  private Obj typeConvert(Obj objVal, ConnPoint pt)
  {
    if (objVal isnot Str) return objVal
    if (pt.rec["curConvert"] != null) return objVal
    
    val := objVal as Str
    ptKind := pt.rec["kind"]   
    switch (ptKind)
    {
      case "Number":
        return Number.makeNum(Float(val))
      case "Bool":
        return Bool(val)
      default:
        return val
    }
  }

//////////////////////////////////////////////////////////////////////////
// Fields
//////////////////////////////////////////////////////////////////////////

  MqttClient? client
  CpMqttPointSubs? subs
  Bool cleanSession := true
  Bool retained := false
  QoS qos := QoS.one
  Duration keepAlive := 30sec
  Str topicRoot := ""
  Str? brokerVersion

}
