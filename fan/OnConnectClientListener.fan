using mqtt

class OnConnectClientListener : ClientListener
{
  new make(CpMqttConn mqttConn)
  {
    this.mqttConn = mqttConn
  }

  private CpMqttConn mqttConn

  override Void onConnected()
  {
    mqttConn.subs.subscribeAllTopics()
  }

}