/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//

using connExt
using mqtt

**
** MqttPointSubs used to keep track of which points
** are subscribed to which topic.
** 
** The MQTT Connector allows for more than one point
** to subscribe to the same topic.
** As points are bound and unbound from topics, this class needs
** to maintain the mapping of topic to points.
** 
** When all points subscribed to a topic have been removed, this class
** will unsubscribe from that topic completely.
**
class CpMqttPointSubs
{

  new make(CpMqttConn mqttConn)
  {
    this.mqttConn = mqttConn
    log = Pod.of(this).log
  }
  
  **
  ** Get the list of points using the supplied topic
  ** 
  ConnPoint[]? getTopicPoints(Str topic)
  {
    return topicPoints[topic]
  }

  **
  ** Add a single point to the list of points using the supplied topic.
  ** If this topic has not yet been subscribed to, create a new point list for the topic.
  ** If the point is already found to be subscribed to the topic, do nothing.
  ** 
  Void addPointTopicSub(Str topic, ConnPoint pt)
  {
    ConnPoint[]? pts := topicPoints[topic]
    if (pts == null) 
    {
      pts = ConnPoint[,]
      topicPoints[topic] = pts
    }
    if (!pts.contains(pt)) pts.add(pt)
  }

  **
  ** Remove a single point to the list of points using the supplied topic.
  ** If there are none, do nothing.
  ** 
  Void removePointTopicSub(Str topic, ConnPoint pt)
  {
    ConnPoint[]? pts := topicPoints[topic]
    if (pts != null) 
    {
      pts.remove(pt)
    }
  }

  **
  ** Get the list of topics that have at least one point bound to it
  ** 
  Str[] listValidTopicSubs()
  {
    validSubs := Str[,]
    topics := topicPoints.keys
    topics.each |topic| 
    {   
      ConnPoint[]? pts := topicPoints[topic]
      if (pts != null && pts.size > 0) 
      {
        validSubs.add(topic)
      }      
    }
    return validSubs
  }

  **
  ** Get the list of topics that are currently subscribed to 
  ** but there are no remaining points bound to that topic
  ** 
  Str[] listInvalidTopicSubs()
  {
    invalidSubs := Str[,]
    topics := topicPoints.keys
    topics.each |topic| 
    {   
      ConnPoint[]? pts := topicPoints[topic]
      if (pts == null || pts.size == 0) 
      {
        invalidSubs.add(topic)
      }      
    }
    return invalidSubs
  }  
  
  **
  ** Clean out any stale topic subscriptions.
  ** This is done in 2 passes.
  ** The first pass uses listInvalidTopicSubs and unsubscribes 
  ** from the returned list of topics.
  ** 
  ** The second pass looks for points whose bound topic has changed,
  ** so it needs to be removed from the list of points for that topic.
  ** If that results in an empty point list, then that topic is unsubscribed.
  ** 
  Void removeInvalidTopicSubs()
  {
    invalidSubs := listInvalidTopicSubs()
    invalidSubs.each |topic| 
    {   
      topicPoints.remove(topic)
      mqttConn.client.unsubscribe(mqttConn.topicRoot + topic).get(10sec)
    }
    
    invalidSubs = Str[,]
    topics := topicPoints.keys
    topics.each |topic| 
    {   
      ConnPoint[]? pts := topicPoints[topic]
      if (pts != null && pts.size > 0) 
      {
        removePts := ConnPoint[,]
        pts.each |pt| 
        { 
          if (pt.rec["cpMqttCur"] != topic) removePts.add(pt)
        }
        pts.removeAll(removePts)
      }     
      if (pts == null || pts.size == 0) 
      {
        invalidSubs.add(topic)
      }      
    }
    invalidSubs.each |topic| 
    {   
      topicPoints.remove(topic)
      mqttConn.client.unsubscribe(mqttConn.topicRoot + topic).get(10sec)
    }
  }
  
  **
  ** Simply unsubscribe from all currently subscribed topics.
  ** This is only used when the MQTT Connector is being closed.
  ** 
  Void clear()
  {
    topics := topicPoints.keys
    topics.each |topic| 
    {  
      mqttConn.client.unsubscribe(mqttConn.topicRoot + topic).get(10sec)
    }
    topicPoints.clear
  }
  
  **
  ** Subscribe to all valid topics
  ** 
  Void subscribeAllTopics()
  {
    topics := listValidTopicSubs()

    topics.each |topic| 
    { 
      fullTopic := mqttConn.topicRoot + topic
      log.info("subscribing to topic [$fullTopic]")
      mqttConn.client.subscribeWith
            .topicFilter(fullTopic)
            .qos(mqttConn.qos)
            .onMessage |tp, msg|
            {
              mqttConn.handleMessage(tp, msg.payload.in.readAllStr)
            }
            .send.get(10sec)
    }
    removeInvalidTopicSubs()    
  }

  **
  ** Create a Str representation of
  ** the Topic:Point[] mapping. 
  ** Used in debug logging.
  ** 
  override Str toStr()
  {
    buf := StrBuf()
    buf.add("{")
    topics := topicPoints.keys
    comma := ""
    topics.each |topic| 
    {
      buf.add(comma + topic + ":")
      ConnPoint[]? pts := topicPoints[topic]
      if (pts != null) 
      {
        buf.add(pts)
      }
      else
      {
        buf.add("NULL")
      }
      comma = ","
    }
    buf.add("}")
    return buf.toStr
  }
  
//////////////////////////////////////////////////////////////////////////
// Fields
//////////////////////////////////////////////////////////////////////////
  Log? log
  CpMqttConn? mqttConn
  Str:ConnPoint[] topicPoints := [:]
  
}
