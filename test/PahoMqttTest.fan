/** 
=====================
The MIT License (MIT)
=====================

Copyright (c) 2016 Crowley Carbon Ltd

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included 
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//   09 Jan 2016  John MacEnri  Creation
//

using haystack
using concurrent
using hisExt
using pahoMqtt
using pointExt
using [java]org.eclipse.paho.client.mqttv3.persist::MemoryPersistence
using [java]org.eclipse.paho.client.mqttv3::MqttClient
using [java]org.eclipse.paho.client.mqttv3::MqttConnectOptions
using [java]org.eclipse.paho.client.mqttv3::MqttMessage
using skyarcd
using folio

**
** PahoMqttTest
**
class PahoMqttTest : skyarcd::ProjTest
{
  // The file <SkySparkHome>/etc/pahoMqttExt/config.props should contain these 3 required external property values
  Uri brokerUri := Uri.fromStr(PahoMqttExt#.pod.config("brokerUri"))
  Str username := PahoMqttExt#.pod.config("brokerUsername")
  Str password := PahoMqttExt#.pod.config("brokerPassword")
  
  Str uuid := Uuid().toStr
  Str clientId := "SkySpark[$uuid]"   
  Str topicRoot := "connTest"
  Int qos := 1
  Bool retain := true

  PahoMqttExt? ext
  MqttClient? client
  Dict? conn

  @DbTest
  Void test()
  {
    buildProj   
    syncAllPoints
    publishVals([1,2,3,4,5], 1)
    Actor.sleep(1000ms)
    verifyPointVals([1,2,3,4,5], 1)
    
    /*
    writePointVals([6], 2)
    Actor.sleep(1000ms)
    verifyPointVals([6], 2)    
    */
  }

  Void buildProj()
  {
    addExt("PahoMqttExt")
    ext = proj.ext("pahoMqtt") as PahoMqttExt
    
    conn = addRec(["id":Ref("conn1"), "pahoMqttConn":Marker.val, "pahoMqttBroker":brokerUri, "pahoMqttCleanSession":false, "pahoMqttQos":1, "username":username, "debug":true])
    proj.passwords.set("conn1", password)
    
    setupClient
    
    addPoint(1, "Number", null,   null, true)
    addPoint(2, "Number", "",     null, true)
    addPoint(3, "Number", "VAL",  null, true)
    addPoint(4, "Number", "CSV",  null, true)
    addPoint(6, "Number", "VAL",  "VAL", false)
  }
  
  Void updateConn(Str defaultParser)
  {
    proj.commit(Diff(proj.read("pahoMqttConn"), ["pahoMqttParser":defaultParser,"pahoMqttConstructor":defaultParser]))
  }
  
  Void setupClient()
  {    
    persistence := MemoryPersistence()
    client = MqttClient(brokerUri.toStr, clientId, persistence)
    connOpts := MqttConnectOptions()
    connOpts.setCleanSession(false)    
    connOpts.setUserName(username)
    connOpts.setPassword(PahoMqttUtils.toCharArray(password))      
    client.connect(connOpts)    
  }

  Void addPoint(Int ptNum, Str kind, Str? parser, Str? constructor, Bool readOnly)
  {
    m := Marker.val
    curTopic := "$topicRoot/points/test/p$ptNum/current"
    
    //We'll publish to the same topic as curVal, so that we get the message back 
    //and curVal should update and we can test the result of publish that way.
    writeTopic := "$topicRoot/points/test/p$ptNum/current"
    
    rec := ["dis":"Point $ptNum", "id":Ref("pt$ptNum"), "point":m, "ro":readOnly, "num":ptNum, "kind":kind, "pahoMqttConnRef":Ref("conn1"), "pahoMqttCur":curTopic]
    if (parser != null) rec.set("pahoMqttParser", parser)
    if (constructor != null) rec.set("pahoMqttConstructor", constructor)    
    if (!readOnly)
    {
      rec.set("writable", m)
      rec.set("pahoMqttWrite", writeTopic)
    }
    addRec(rec)          
  }
  
  Void syncAllPoints()
  {
    ext.syncCur(proj.readAllList("point and pahoMqttCur"))
  }
    
  Void publishVals(Int[] ptNums, Int scale)
  {
    ptNums.each |ptNum| { publishVal(ptNum, ptNum * scale)  }
  }
  
  Void publishVal(Int ptNum, Int val)
  {
    pt := proj.read("point and num==$ptNum")
    topic := "$topicRoot/points/test/p$ptNum/current"
    clearRetained(topic)
    
    msg := MqttMessage(PahoMqttUtils.toByteArray("$val")) 
    if (pt["pahoMqttParser"] != null && pt["pahoMqttParser"] == "CSV")
    {
      msg = MqttMessage(PahoMqttUtils.toByteArray("$val,ok"))
    }

    msg.setQos(qos)
    msg.setRetained(retain)
    client.publish(topic, msg)
  }
  
  Void clearRetained(Str topic)
  {
    msg := MqttMessage(PahoMqttUtils.toByteArray("")) 
    msg.setRetained(true)
    client.publish(topic, msg)
  }

  Void verifyPointVals(Int[] ptNums, Int scale)
  { 
    ptNums.each |ptNum| { verifyPointVal(proj.read("point and num==$ptNum"), ptNum * scale)  }
  }
  
  Void verifyPointVal(Dict pt, Int val)
  {
    verifyEq(pt["curVal"], n(val)) 
  }
  
  Void writePointVals(Int[] ptNums, Int scale)
  { 
    ptNums.each |ptNum| { writePointVal(ptNum, ptNum * scale)  }
  }
  
  Void writePointVal(Int ptNum, Int val)
  { 
    eval("read(point and num==$ptNum).pointOverride($val)")
  }


}