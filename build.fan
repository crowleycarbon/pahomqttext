#! /usr/bin/env fan

//
// History:
//  Apr 2020 John MacEnri Creation
//

using build

class Build : BuildPod
{
  new make()
  {
    podName = "cpMqttExt"
    summary = "Cool Planet MQTT Connector Extension"
    version = Version("2.1.3")
    meta    = [
                "org.name":     "Cool Planet Clarity Ltd",
                "license.name": "MIT",
                "skyspark.docExt": "true"
               ]
    depends  = ["sys 1.0",
                "axon 3.1",
                "concurrent 1.0",
                "connExt 3.1",
                "dom 1.0",
                "folio 3.1",
                "haystack 3.1",
                "hisExt 3.1",
                "hxPoint 3.1 ",
                "skyarc 3.1",
                "skyarcd 3.1",
                "util 1.0",
                "mqtt 3.1",
                "crypto 1.0",
                "inet 1.0"
               ]
    srcDirs = [`fan/`]
    resDirs = [`lib/`, `locale/`]
    docApi  = true
    index   =
    [
      "skyarc.ext": "cpMqttExt::CpMqttExt",
      "skyarc.lib": "cpMqttExt::CpMqttLib",
    ]
  }
}